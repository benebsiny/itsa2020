// An array stores if field is passed
const isPass = [false, false, false, false, false, false];

// The following code do validattion
$('#integer').on('keyup', function (event) {
    if (!(49 <= event.keyCode && event.keyCode <= 57)) {
        return false;
    }
});

$('#integer').on('change', function (event) {
    const value = event.target.value;
    if (!(0 <= value && value <= 10)) {
        error(0);
    } else {
        pass(0);
    }
});

$('#name').on('change', function (event) {
    const value = event.target.value;
    const errElement = $('.err-msg[data-num="1"]');

    if (value.match(/[^A-Za-z0-9]+/)) {
        errElement.text('不正確的文數字格式');
        error(1);
    } else if (value.length < 6) {
        errElement.text('請輸入英文且字數需大於六!');
        error(1);
    } else {
        pass(1);
    }
});

$('#username').on('change', function (event) {
    const value = event.target.value;
    if (value.match(/[^A-Za-z0-9]+/)) {
        $('.err-msg[data-num="2"]').text('不正確的文數字格式');
        error(2);
    } else if (value.length < 5) {
        $('.err-msg[data-num="2"]').text('請輸入英文且字數需大於5');
        error(2);
    }
    else {
        pass(2);
    }
});

$('#email').on('change', function (event) {
    const value = event.target.value;

    if (!value.match(/^[a-zA-Z0-9]+@[a-zA-Z0-9]+(\.[a-zA-Z0-9]+)+$/)) {
        error(3);
    } else {
        pass(3);
    }
});

$('#password').on('change', function (event) {
    const value = event.target.value;
    if (value.length < 9) {
        error(4);
    } else {
        pass(4);
    }
});

$('#password-again').on('change', function (event) {
    const value = event.target.value;

    if ($('#password').val() !== value) {
        error(5);
    } else {
        pass(5);
    }
});

// If error, show error message and hide tick
function error(number) {
    $(`.err-msg[data-num="${number}"]`).show();
    $(`.tick[data-num="${number}"]`).css('visibility', 'hidden');

    isPass[number] = false;
    checkIfCanSubmit();
}

// If pass, hide error message and show tick
function pass(number) {
    $(`.err-msg[data-num="${number}"]`).hide();
    $(`.tick[data-num="${number}"]`).css('visibility', 'visible');

    isPass[number] = true;
    checkIfCanSubmit();
}

// If all fields are pass, you can submit it.
function checkIfCanSubmit() {
    for (let i = 0; i < isPass.length; i++) {
        if (isPass[i] === false) {
            $('.submit').attr('disabled', 'true');
            return;
        }
    }
    $('.submit').removeAttr('disabled');
}