// Select today
$('#today-btn').on('click', function () {
    const today = new Date();
    $('#date').val(today.getDate());
    $('#month').val(today.getMonth() + 1);
    $('#year').val(today.getFullYear());
})

// Do validate
$('#date').on('change', function (event) {
    const date =  parseInt(event.target.value);
    const month = parseInt($('#month').val());
    const year = parseInt($('#year').val());

    validateDate(date, month, year);
});

$('#month').on('change', function (event) {
    const date = parseInt($('#date').val());
    const month = parseInt(event.target.value);
    const year = parseInt($('#year').val());

    validateDate(date, month, year);
});

$('#year').on('change', function (event) {
    const date = parseInt($('#date').val());
    const month = parseInt($('#month').val());
    const year =  parseInt(event.target.value);

    validateDate(date, month, year);
});

// Check if date is correct
function validateDate(date, month, year) {
    const monthToDate = [-1, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    console.log(date, month, year);
    if (isLeap(year)) {
        monthToDate[2] = 29;
    } else {
        monthToDate[2] = 28;
    }

    if (date > monthToDate[month]) {
        alert("Error Date!!")
    }
}

function isLeap(year) {
    if ((year % 4 === 0 && year % 100 !== 0) || year % 400 === 0) {
        return true;
    } else {
        return false;
    }
}